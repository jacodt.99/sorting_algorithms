cc = g++
binDir = ./bin/
includeDir = ./include/
srcDir = ./src/
cflags = -Wall
obj = $(binDir)main.o $(binDir)sorting_algorithms.o


$(binDir)output: $(obj)
	$(cc) $(cflags) $? -o $(binDir)output

$(binDir)main.o: $(srcDir)main.cpp
	$(cc) -c $< -o $@

$(binDir)sorting_algorithms.o: $(srcDir)sorting_algorithms.cpp $(includeDir)sorting_algorithms.h
	$(cc) -c $< -o $@

.PHONY: clean cleanall

clean:
	rm -rf $(binDir)*.o

cleanall:
	rm -rf $(binDir)*.o output