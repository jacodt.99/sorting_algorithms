

#include "../include/sorting_algorithms.h"
#include <iostream>
#include <ctime>

#define ARR_LEN 20

using namespace std;

static void printArr(int[], int);


/**
*   drivers
*/
int main(void){

    double time_spent = 0.0;

    int a[ARR_LEN] = {4,2,5,9,1,7,95,34,19,435,3,433,12,212,2112,11,93, 42, 322, 10};

    printArr(a, ARR_LEN);

    clock_t begin = clock();
    countingSort(a, ARR_LEN);
    clock_t end = clock();

    printArr(a, ARR_LEN);


    time_spent += (double)(end - begin) / CLOCKS_PER_SEC;

	printf("tempo impiegato:  %f seconds", time_spent);
    cout << "\n";
    return 0;
}

static void printArr(int a[], int n){
    for(int i = 0; i < n; i++){
        cout << a[i] << " ";
    }
    cout << "\n";
}