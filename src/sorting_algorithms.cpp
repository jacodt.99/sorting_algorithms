#include "../include/sorting_algorithms.h"

/**
*   algoritmo di ordinamento selection sort.
*   1) si setta l'elemento selezioato (all'inizio a[0]) come minimo\n
*   2) si ciclano tutti gli elementi successivi in cerca di uno minore\n
*   3) si sostituisce l'elemento selezionato con il minimo\n
*   4) si incrementa l'indice dell'elemento selezionato\n
*   5) si ripete da 1) fino a che non si è ciclano tutto l'array - 1\n\n
*   
*   costo computazionale:\n 
*   time: best: Ω(n^2), avg: θ(n^2) , worst: O(n^2)\n 
*   space: worst O(1)
*
*   @param a array in input
*   @param n lunghezza di a
*/
void selectionSort(int a[], int n){
    int minI;
    for (int i = 0; i < n - 1; i++){
        minI = i;
        for (int j = i + 1; j < n; j++){
            if (a[j] < a[minI]){
                minI = j;
            }
        }
        if(minI != i)
            swap(a[i], a[minI]);
    }
}

static void swap(int &a, int &b){
    int temp = a;
    a = b;
    b = temp;
}

/**
*   algoritmo di ordinamento insertion sort.
*   1) si seleziona l'elemento (da a[1]) salvandolo in key\n
*   2) si setta un altro indice (j) per scorrere la lista indietro\n
*   3) si scorre indietro finchè non si è alla fine o si è trovato il posto giusto per key, shiftando ogni volta l'array a destra per fare posto a key\n
*   4) una volta trovato il posto giusto per key, lo si posiziona\n
*   5) si incrementa l'indice dell'elemento selezionato\n
*   6) si ripete da 1) fino a che non si è ciclano tutto l'array - 1\n\n
*   
*   costo computazionale:\n 
*   time: best: Ω(n), avg: θ(n^2), worst: O(n^2)\n 
*   space: worst: O(1)
*
*   @param a array in input
*   @param n lunghezza di a
*/
void insertionSort(int a[], int n){
    int key, j;
    for (int i = 1; i < n; i++){
        key = a[i];
        j = i - 1;
        while(j >= 0 && key < a[j]){
            a[j + 1] = a[j];
            j--;
        }
        a[j + 1] = key;
    }
}

/**
*   algoritmo di ordinamento merge sort.
*   1) si divide a metà finche non rimane solo un elemento (costo O(log(n)))\n
*   2) si uniscono gli elementi in ordine metà per metà, ricombinandoli il costo di ordinamento è O(n) visto che le metà sono già ordinate\n\n
* 
*   chaiamta principale: mergeSort(a, 0, ARR_LEN - 1)\n\n
*   
*   costo computazionale:\n 
*   time: best: Ω(n*log(n)), avg: θ(n*log(n)) , worst: O(n*log(n))\n 
*   space: worst O(n)
*
*   @param a array in input
*   @param l indice primo elemento
*   @param r indice ultimo elemento\n\n
*
*/
void mergeSort(int a[], int l, int r) 
{ 
    if (l < r) {
        int m = l + (r - l) / 2; 
        mergeSort(a, l, m); 
        mergeSort(a, m + 1, r); 
        merge(a, l, m, r); 
    } 
}

static void merge(int arr[], int l, int m, int r) 
{ 
    int i, j, k; 
    int n1 = m - l + 1; 
    int n2 = r - m; 
    int L[n1], R[n2]; 

    for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1 + j]; 

    i = 0; 
    j = 0; 
    k = l; 

    while (i < n1 && j < n2) { 
        if (L[i] <= R[j]) { 
            arr[k] = L[i]; 
            i++; 
        } 
        else { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 

    while (i < n1) { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 
  
    while (j < n2) { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
} 

/**
*   algoritmo di ordinamento quick sort.
*   1) uso l'ultimo elemento come pivot\n
*   2) attraverso partition, sposto tutti gli elementi minori del pivot a sinistra, e tutti i maggiori a destra\n 
*   3) ripeto ricorsivamente sulla metà a sinistra e sulla metà a destra\n\n
*
*   chaiamta principale: quickSort(a, 0, ARR_LEN - 1)\n\n
*   
*   costo computazionale:\n 
*   time: best: Ω(n*log(n)), avg: θ(n*log(n)) , worst: O(n^2)\n 
*   space: worst O(log(n))
*
*   @param a array in input
*   @param l indice primo elemento
*   @param h indice ultimo elemento\n\n
*
*/
void quickSort(int arr[], int l, int h){  
    if (l < h) {  
        int pi = partition(arr, l, h);  
        quickSort(arr, l, pi - 1);  
        quickSort(arr, pi + 1, h);  
    }  
} 

static int partition (int arr[], int l, int h){  
    int pivot = arr[h]; // pivot  
    int i = (l - 1); // Index of smaller element    
    for (int j = l; j <= h - 1; j++)  {  
        if (arr[j] < pivot){  
            i++; // increment index of smaller element  
            swap(arr[i], arr[j]);  
        }  
    }  
    swap(arr[i + 1], arr[h]);  
    return (i + 1);  
}

/**
*   algoritmo di ordinamento quick sort.
*   1) trovo l'elemento più grande nell'array\n
*   2) inizializzo un array count contenente il numero di occorenze di ogni elemento\n
*   3) calcolo la somma accumulativa per ogni elemento\n
*   4) costruisco l'array ordinato, come indice il valore nell'array count - 1 e come valore il valore nell'array originario, in ordine inverso\n
*   5) copio l'array temporaneo in quello originale\n\n
*   
*   costo computazionale:\n 
*   time: best: Ω(n + k), avg: θ(n + k) , worst: O(n + k)\n 
*   space: worst O(k)\n\n
*
*   notare che il costo computazionale dipende da k, ovvero l'elemento più grande dell'array
*
*   @param a array in input
*   @param n lunghezza di a
*
*/
void countingSort(int a[], int n) {

  int* tempArr = new int[n];
  int* count = new int[n];
  int max = a[0];

  for (int i = 1; i < n; i++) {
    if (a[i] > max)
      max = a[i];
  }

  for (int i = 0; i <= max; i++) {
    count[i] = 0;
  }

  for (int i = 0; i < n; i++) {
    count[a[i]]++;
  }

  for (int i = 1; i <= max; i++) {
    count[i] += count[i - 1];
  }

  for (int i = n - 1; i >= 0; i--) {
    tempArr[count[a[i]] - 1] = a[i];
    count[a[i]]--;
  }

  for (int i = 0; i < n; i++) {
    a[i] = tempArr[i];
  }
}

