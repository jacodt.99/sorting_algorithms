#ifndef SORTING_ALGORITHMS_H
#define SORTING_ALGORITHMS_H

void selectionSort(int[], int);
void insertionSort(int[], int);
void mergeSort(int[], int, int);
void quickSort(int[], int, int);
void countingSort(int[], int);

static void swap(int&, int&);
static void merge(int[], int, int, int);
static int partition(int[], int, int);

#endif // SORTING_ALGORITHMS_H